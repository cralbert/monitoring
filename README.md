**1. Resolución de problemas de programación y diseño 
    A) Tome el código que sigue – que representa una cuenta bancaria – y responda las siguientes preguntas: 
    ¿Le parece que está bien diseñada la clase? Si no lo está, pero funciona, ¿la cambiaría o agregaría clases nuevas?**

    
    En principio agregaría un enum de tipo de cuenta, para que la responsabilidad y control del tipo de cuenta quede encapsulado en una única clase y no en constantes con valores que pueden cambiar en el tiempo. Si en el futuro se desea agregar/modificar/eliminar algún tipo, solo se modificaria la clase enum.

**Si cambiaría el diseño, exponga todo lo que cambiaría con un diagrama de clases de UML. Si algún método debe cambiarlo mucho, escríbalo.**



**¿Es seguro hacer estos cambios? ¿Por qué? ¿Qué precauciones tomaría?**

En principio la clase Cuenta no debería contener lógica del negocio, debería representar datos, información. Para poder manejar los cambios en el tiempo es necesario un gestor de la cuenta que maneje el comportamiento y lógica del negocio.
Otra opción es generar clases por cada tipo, heredando de una clase cuenta abstracta con los valores en comun

**¿Agregaría getters y setters? ¿Cuáles? ¿Por qué?**
 
En cuanto a los getters y setters, todos los atributos podrían tener todos los getters. En el caso de los setters, atributos como tipo de cuenta, numero, no deberían cambiar luego de creado el objeto. En cambio nombre del titular si por ej.
 
**¿Su solución usa algún patrón de diseño? ¿Cuál?**
 
Esto depende de la lógica del negocio y la complejidad del diseño, se podria agregar patrones creacionales como factory

**B) Código.**

Existen 4 sensores en un sistema que miden un valor numérico y deben enviarlo para su procesamiento. El sistema de monitoreo, toma estos valores y calcula tres parámetros: promedio, valor máximo y valor mínimo buscando alguna de las siguientes anomalías:

*  La diferencia entre el valor mínimo y máximo recibido sea mayor a una constante S(configurable)
* El valor promedio sea superior a una constante M (configurable)

En caso de detectar alguna de las situaciones mencionadas en los puntos anteriores, debe mostrar por pantalla un mensaje de error indicando esta situación.
Es importante tener en cuenta que:

* Los sensores envían 2 mediciones por segundo (en forma independiente y potencialmente simultánea).
* El sistema de procesamiento, por limitaciones de hardware, sólo puede procesar información 2 veces por minuto.
* Se debe respetar el orden de ingreso de los mensajes al sistema de monitoreo.
* Todos los mensajes recibidos deben ser loggeados asi como también registrar información al momento de su procesamiento.
    
En Java o C#, desarrolle un programa que se ejecute desde consola y que modele este sistema.
Para probarlo,

*  Escribir al menos dos tests que validen la funcionalidad alguna de las funcionalidades requeridas.
*  Desde la consola se deberá poder ejecutar un caso en el los 4 sensores generen información aleatoria que será procesada por el sistema de monitoreo.

PLUS: Permitir que el sistema de monitoreo reciba los mensajes mediante HTTP.

    El ejercicio se desarrollo en java, maven para la gestion de librerias, springboot como framework, para facilitar el desarrollo de servicios restful y de scheduled task
    Ademas de desarrollar test unitarios basicos, se agrego una clase scheduler que simula los 4 sensores, lo que permite realizar testeo de volumen y procesamiento ademas del testeo de funcionamiento correcto.

**2. Aspectos conceptuales 
    A)Explique el uso del patrón Strategy. Una vez explicado, conteste: ¿Cuántas instancias necesita de cada clase de estrategia?
    ¿Hay algún otro patrón que lo ayude en esto? Si lo hay, muestre un pequeño ejemplo en código.**

El patrón Strategy permite aplicar distinta lógica de negocio o distintos algoritmos dependiendo de factores que determinan que estrategia aplicar.
Para cada estrategia se debe aplicar una instancia.
Un patrón que a veces puede intervenir es Factory, determinando que estrategia instanciar de acuerdo a los parámetros de entrada.

Ej: Contexto: Empresa de pedidos que genera ordenes de compra. Dependiendo del cliente y la forma de pago a veces puede aplicar un descuento en la orden (Por cuestiones de espacio, se obvian los getters y setters, además de clases enum)

```
    public class Client {
    	private String name;
    	private ClientType type;
    }
    
    public class Order {
    	private Client client;
    	private Double total;
    	private Double discount;
    	private Payment payment;
    }
    
    public interface DiscountStrategy {
    
    	public Double calculateDiscount(Order order);
    }
    
    public class DicountByCashPayment implements DiscountStrategy {
    
    	public Double calculateDiscount(Order order) {
    		return order.getTotal() * 0.15;
    	}
    
    }
    
    public class DiscountByClient implements DiscountStrategy {
    
    	public Double calculateDiscount(Order order) {
    		return order.getTotal() * 0.10;
    	}
    
    }
    
    public class WithoutDiscount implements DiscountStrategy{
    
    	public Double calculateDiscount(Order order) {
    		return 0d;
    	}
    
    }
    
    public class FactoryDiscountStrategy {
    
    	private static final FactoryDiscountStrategy factory = new FactoryDiscountStrategy();
    	
    	private FactoryDiscountStrategy() {
    		
    	}
    	
    	public static FactoryDiscountStrategy instance() {
    		return factory;
    	}
    	
    	public DiscountStrategy getStrategy(Order order) {
    		if (order.getClient().getType().equals(ClientType.FINAL)) {
    			return new DiscountByClient();
    		} else if (order.getPayment().equals(Payment.CASH)) {
    			return new DicountByCashPayment();
    		} else {
    			return new WithoutDiscount();
    		}
    	}
    }
    
    public class Context {
    
    	public static void main(String[] args) {
    		
    		Client client = new Client();
    		client.setName("Nombre");
    		client.setType(ClientType.FINAL);
    		
    		Order order = new Order();
    		order.setClient(client);
    		order.setPayment(Payment.ACOUNT);
    		order.setTotal(1000d);
            order.setDiscount(FactoryDiscountStrategy.instance().getStrategy(order).calculateDiscount(order));
    	}
    }
```

**Enumere todas las ventajas que conozca de escribir pruebas unitarias automatizadas antes de escribir el código funcional.**


*  Permite contemplar los casos validos que se pueden llegar a presentar

*  Al tener pruebas anteriores al desarrollo, permite limitar el alcance del desarrollo

*  A medida que se va avanzando en el desarrollo, se puede ir verificando que cumpla cada uno de las pruebas, y que cualquier cambio que se realice no afecte las pruebas ya testeadas.

**B) ¿Cuándo utiliza el patrón Observador? ¿Qué ventajas tiene?**

    El patrón Observador se utiliza cuando un objeto necesita conocer cuando cambia el contexto de otro objeto para asi realizar alguna acción. La ventaja de este patrón esta en que la responsabilidad de iniciar las acciones se derivan a la clase del cual se necesita conocer cuando cambia, por lo que el objeto observador no tiene que estar pendiente de dichos cambios.

**4.	Bases de datos y SQL**

Usuario 
id integer username varchar(255) 
password varchar(255) 

Persona 
id integer 
idUsuario integer 
nombre varchar(255) 
apellido varchar(255) 
fechaNac date 

Usando SQL standard, defina la consulta SQL que 
A) Devuelva los usuarios cuyos nombre de persona empiecen por "Jorg" 

    Select u.*
    From Persona p
    Join Usuario u on u.id = p.idUsuario
    Where p.nombre like “Jorg%”;


Devuelva los meses en los cuales la cantidad de usuarios que cumplen años es mayor a 10.

    Select month(p.fechaNac), count(month(p.fechaNac) cant
    From Usuario u
    Join Persona p on p.idUsuario = u.id
    Group by month(p.fechaNac)
    Having cant > 10

