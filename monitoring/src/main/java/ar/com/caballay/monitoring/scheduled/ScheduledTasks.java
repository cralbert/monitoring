package ar.com.caballay.monitoring.scheduled;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ar.com.caballay.monitoring.services.MonitoringSystem;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	@Autowired
	private MonitoringSystem monitor;
	
    @Scheduled(fixedRate = 30000)
    public void reportCurrentTime() {
        try {
        	monitor.calculate();
		} catch (Exception e) {
			log.error("Error calculating values.", e);
		}
    }
    
}
