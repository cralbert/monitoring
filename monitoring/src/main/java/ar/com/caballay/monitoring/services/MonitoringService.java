package ar.com.caballay.monitoring.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MonitoringService {

	private static final Logger log = LoggerFactory.getLogger(MonitoringService.class);
	
	@Autowired
	private MonitoringSystem monitor;
	
    @PutMapping("/register/{value}")
    @ResponseBody
    public ResponseEntity<String> register(@PathVariable Double value) {
    	log.info("Valor sensor registrado: " + value);
    	monitor.addValue(value);
    	return new ResponseEntity<String>("Put Response", HttpStatus.OK);
    }
    
    @PutMapping("/valueM/{value}")
    @ResponseBody
    public ResponseEntity<String> setValueM(@PathVariable Double value) {
        log.info("Valor M registrado: " + value);
        monitor.consM(value);
        return new ResponseEntity<String>("Put Response", HttpStatus.OK);
    }
    
    @PutMapping("/valueS/{value}")
    @ResponseBody
    public ResponseEntity<String> setValueS(@PathVariable Double value) {
        log.info("Valor S registrado: " + value);
        monitor.consS(value);
        return new ResponseEntity<String>("Put Response", HttpStatus.OK);
    }
}
