package ar.com.caballay.monitoring.scheduled;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ar.com.caballay.monitoring.services.MonitoringSystem;

@Component
public class SensorsTask {

	private static final Logger log = LoggerFactory.getLogger(SensorsTask.class);
	private static final Double constM = 0.10;
	private static final Double constS = 0.5;
	
	@Autowired
	private MonitoringSystem monitor;
	
	 @PostConstruct
     public void initializeValues() {
		 monitor.consM(constM);
		 monitor.consS(constS);
     }
	 
    @Scheduled(fixedRate = 500)
    public void sensor1() {
        try {
        	monitor.addValue(Math.random());
		} catch (Exception e) {
			log.error("Error calculating values.", e);
		}
    }
    
    @Scheduled(fixedRate = 500)
    public void sensor2() {
        try {
        	monitor.addValue(Math.random());
		} catch (Exception e) {
			log.error("Error calculating values.", e);
		}
    }
    
    @Scheduled(fixedRate = 500)
    public void sensor3() {
        try {
        	monitor.addValue(Math.random());
		} catch (Exception e) {
			log.error("Error calculating values.", e);
		}
    }
    
    @Scheduled(fixedRate = 500)
    public void sensor4() {
        try {
        	monitor.addValue(Math.random());
		} catch (Exception e) {
			log.error("Error calculating values.", e);
		}
    }
}
