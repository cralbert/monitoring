package ar.com.caballay.monitoring.services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class MonitoringSystem {

	private static final Logger log = LoggerFactory.getLogger(MonitoringSystem.class);
	
	private Double consS, consM;
	private Collection<Double> values = Collections.synchronizedCollection(new ArrayList<Double>());
	
	public MonitoringSystem consS(double consS) {
		this.consS = consS;
		return this;
	}
	
	public MonitoringSystem consM(double consM) {
		this.consM = consM;
		return this;
	}
	
	public Double consM() {
		return consM;
	}
	
	public Double consS() {
		return consS;
	}
	
	public synchronized void addValue(double value) {
		values.add(value);
	}
	
	public boolean calculate() throws RuntimeException {
		log.info("Moinitoring system is calculating values...");
		if (consS == null || consM == null) {
			log.error("Must define constant M and/or constand S");
			return false;
		}
		if (values.size() == 0) {
			log.error("Monitoring system has not registered sensor values.");
			return false;
		}
		List<Double> tempValues = new ArrayList<>(values);
		values.clear();
		double average = 0;
		double min = tempValues.get(0);
		double max = 0;
		for (Double value : tempValues) {
			average +=value;
			min = min > value ? value : min;
			max = max < value ? value : max;
		}
		average /= tempValues.size();
		if (max - min > consS || average > consM) {
			log.error(String.format("Exist an anomaly in any of the following values. Difference between max and min is %f. Average is %f", max - min, average));
			return false;
		}
		log.info("Moinitoring system. Values {} - Max {} - Min {} - Average {}", tempValues, max, min, average);
		return true;
	}
}
