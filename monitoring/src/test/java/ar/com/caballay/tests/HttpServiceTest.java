package ar.com.caballay.tests;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.net.URI;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import ar.com.caballay.monitoring.services.MonitoringService;
import ar.com.caballay.monitoring.services.MonitoringSystem;

@RunWith(SpringRunner.class)
// @SpringBootTest(classes = MonitoringService.class)
@SpringBootTest(classes = MonitoringService.class)
@WebAppConfiguration
public class HttpServiceTest {

	private MockMvc mockMvc;

	@MockBean
	private MonitoringSystem monitor;

	@Autowired
	private WebApplicationContext webAppConfiguration;

	@Before
	public void init() {
		mockMvc = webAppContextSetup(webAppConfiguration).build();
	}

	@Test
	public void setValueConstantsTest() throws Exception {
		Double valueM = 12d;
		Double valueS = 15d;
		String baseUrl = "http://localhost:" + 8080 + "/valueM/" + valueM;
		URI uri = new URI(baseUrl);
		mockMvc.perform(put(uri)).andExpect(status().isOk());
		baseUrl = "http://localhost:" + 8080 + "/valueS/" + valueS;
		uri = new URI(baseUrl);
		mockMvc.perform(put(uri)).andExpect(status().isOk());
		assertEquals(valueS, monitor.consS());
	}
}
