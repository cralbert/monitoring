package ar.com.caballay.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import ar.com.caballay.monitoring.services.MonitoringSystem;

@RunWith(SpringRunner.class)
public class MonitoringSystemTest {

	@TestConfiguration
    static class MonitoringSystemTestContextConfiguration {
  
        @Bean
        public MonitoringSystem monitoringService() {
            return new MonitoringSystem();
        }
    }
	
	@Autowired
	private MonitoringSystem monitor;
	
	@Rule
	public ExpectedException expectedEx = ExpectedException.none();
	
	@Test
	public void calculateOk() throws Exception {
		monitor.consM(10);
		monitor.consS(10);
		monitor.addValue(1);
		monitor.addValue(2);
		monitor.addValue(3);
		monitor.addValue(4);
		assertEquals(monitor.calculate(), true);
	}
	
	@Test
	public void calculateNotOk() throws Exception {
		monitor.consM(10);
		monitor.consS(10);
		monitor.addValue(1);
		monitor.addValue(2);
		monitor.addValue(3);
		monitor.addValue(20);
		assertEquals(monitor.calculate(), false);
	}

	@Test
	public void calculateWithoutCons() throws Exception {
		monitor.addValue(1);
		monitor.addValue(2);
		monitor.addValue(3);
		monitor.addValue(4);
		assertEquals(monitor.calculate(), false);
	}
	
	@Test
	public void calculateWithoutValues() throws Exception {
		monitor.consM(10);
		monitor.consS(10);
		assertEquals(monitor.calculate(), false);
	}
}
